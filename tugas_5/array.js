console.log("soal 1")
function range(startNum, finishNum) {
    if (startNum == null || finishNum == null)
      return -1
    jumlah = []
    for(var angka = startNum; angka <= finishNum; angka++) {
    jumlah.push(angka)
    }
  return jumlah
  }
  
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
  
console.log('=====================================')
console.log("\n soal 2")
  
  function rangeWithStep(startNum, finishNum, step) {
    jumlah = []
    if (startNum == null || finishNum == null || step == null){
      return 1
    } else
      if(startNum > finishNum){
        for(var i = startNum; i >= finishNum; i-= step){
        jumlah.push(i)
        }
        return jumlah
      }
      
    for(var angka = startNum; angka <= finishNum; angka+= step){
    jumlah.push(angka)
    }
  return jumlah
  }
  
  console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
  console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
  console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
  console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]
  
  console.log('=====================================')
  console.log("\n soal 3")
  
  function sum(startNum, finishNum, step) {
    arr = Array()
    jumlah = 0;
    if (startNum == null || finishNum == null || step == null){
      return 1
    } else
      if(startNum > finishNum){
        for(var i = startNum; i >= finishNum; i-= step){
        jumlah += i
        arr.push(i)
        }
        return arr
      }
      
    
    for(var angka = startNum; angka <= finishNum; angka+= step){
    jumlah += angka
    arr.push(angka)
    }
  return arr
  }
  
  console.log(sum(1, 10));
  console.log(sum(5,50,2));
  console.log(sum(15,10));
  console.log(sum(20, 10, 2));
  console.log(sum(1));
  console.log(sum());
  
  console.log('=====================================')
  console.log("\n soal 4")
  
  var input = [
                  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
              ];
  
  var id = [];
  var nama = [];
  var kota = [];
  var ttl = [];
  var hobby = [];
  
  for (var i = 0; i < input.length; i++){
    for(var j = 0; j <= 0; j++){
      id.push(input[i][j])
    }
  }
  
  for (var i = 0; i < input.length; i++){
    for(var j = 1; j <= 1; j++){
      nama.push(input[i][j])
    }
  }
  
  for (var i = 0; i < input.length; i++){
    for(var j = 2; j <= 2; j++){
      kota.push(input[i][j])
    }
  }
  
  for (var i = 0; i < input.length; i++){
    for(var j = 3; j <= 3; j++){
      ttl.push(input[i][j])
    }
  }
  
  for (var i = 0; i < input.length; i++){
    for(var j = 4; j <= 4; j++){
      hobby.push(input[i][j])
    }
  }
  
  function dataHandling(){
    for (var i = 0; i < input.length; i++){
     var cetak = console.log("Nomer Id : "+ id[i]+
                  "\n Nama Lengkap : "+nama[i]+
                            "\n TTL : "+kota[i]+" "+ttl[i]+
                            "\n Hobby : "+hobby[i])
    }
    return cetak
  }
  
  console.log(dataHandling())

  console.log('=====================================')
  console.log("\n soal 5")

  function balikKata(str){
    return str.split('').reverse().join('')
  }

  console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers")) 

     
  
  console.log('=====================================')
  console.log("\n soal 6")
  
  var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
  
  function dataHandling2(masuk){
    var data = masuk;
    data.push(data.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria"
               ,"SMA Internasional Metro"))
    var pisah = data[3].split("/")
    console.log(pisah[1])
    var gabung = pisah.join("-")
    console.log(gabung)
    
   
    return data
  }
  
  console.log(dataHandling2(input))
  
  