console.log('soal1')
var flag = 2;
while(flag <= 20) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag+' - I love coding'); // Menampilkan nilai flag pada iterasi tertentu
  flag += 2; // Mengubah nilai flag dengan menambahkan 2 
}

var flag = 20;
while(flag >= 2) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag+' - I will become a mobile developer'); // Menampilkan nilai flag pada iterasi tertentu
  flag -= 2; // Mengubah nilai flag dengan mengurangi 2 
}

console.log('---------------------------------------------')
console.log('soal2')

for(var angka = 1; angka <= 20; angka++){
  if((angka%2)==0){
    console.log(angka + ' - berkualitas ');
  }else if((angka%3)==0 && (angka%2)==1){
    console.log(angka + ' - I love coding ');
  }else{
    console.log(angka + ' - santai ');
  }
}

console.log('---------------------------------------------')
console.log('soal3')

var pagar = '####'

for (var angka = 1; angka <=4; angka++){
  console.log(pagar)
}

console.log('---------------------------------------------')
console.log('soal4')

var pagar = '#'

for (var angka = 1; angka <=7; angka++){
  console.log(pagar)
  pagar = pagar + '#'
}

console.log('---------------------------------------------')
console.log('soal5')

for (var i = 1; i<= 8; i++){
  if(i%2 == 0){
    console.log('# # # #');
  }else{
    console.log(' # # # #')
  }
}