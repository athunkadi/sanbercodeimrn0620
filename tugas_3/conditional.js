/*
Soal if-else
*/

const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("Siapa nama kamu ? ", function(name) {
    rl.question("apa peran kamu ? ", function(peran) {
        if (name == ''){
            console.log('nama harus diisi')
        } else if (peran == ''){
            console.log(`Halo ${name}, Pilih peranmu untuk memulai game!`)
        } else if (peran == 'penyihir'){
            console.log(`Selamat datang di Dunia Werewolf, ${name}`)
            console.log(`Halo Penyihir ${name}, kamu dapat melihat siapa yang menjadi werewolf!`)
        }else if (peran == 'Guard'){
            console.log(`Selamat datang di Dunia Werewolf, ${name}`)
            console.log(`Halo Guard ${name},  kamu akan membantu melindungi temanmu dari serangan werewolf.`)
        }else if (peran == 'Werewolf'){
            console.log(`Selamat datang di Dunia Werewolf, ${name}`)
            console.log(`Halo Werewolf ${name}, Kamu akan memakan mangsa setiap malam!`)
        }else{
            console.log(`hi ${name}, Pilihan peranmu tidak ada`)
        }
        rl.close();
    });
});

var tanggal = 16;
var bulan = 6;
var tahun = 2020;

switch(bulan) {
    case 1:
        bulan = 'januari';
        break;
    case 2:
        bulan = 'febuari';
        break;
    case 3:
        bulan = 'maret';
        break;
    case 4:
        bulan = 'april';
        break;
    case 5:
        bulan = 'mei';
        break;
    case 6:
        bulan = 'juni';
        break;
    case 7:
        bulan = 'juli';
        break;
    case 8:
        bulan = 'agustus';
        break;
    case 9:
        bulan = 'september';
        break;
    case 10:
        bulan = 'oktober';
        break;
    case 11:
        bulan = 'november';
        break;
    case 12:
        bulan = 'desember';
        break;
    default:
        bulan = 'tidak terdaftar';

console.log(tanggal+' '+bulan+' '+tahun);