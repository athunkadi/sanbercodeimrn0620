console.log('\n soal 1')
function teriak() {
    return "Halo Sanbers!"
  }
   
  console.log(teriak())

  console.log('==============================')
  console.log('\n soal 2')

  function kalikan(num1, num2) {
    hasil = num1 * num2
    return hasil
  }
  var num1 = 12
  var num2 = 4
   
  var hasilKali = kalikan(num1, num2)
  console.log(hasilKali)

  console.log('==============================')
  console.log('\n soal 3')

  function introduce(name, age, address, hobby) {
    hasil = ("Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!")
    return hasil
  }
  var name = "Agus"
  var age = 30
  var address = "Jln. Malioboro, Yogyakarta"
  var hobby = "Gaming"
   
  var perkenalan = introduce(name, age, address, hobby)
  console.log(perkenalan)