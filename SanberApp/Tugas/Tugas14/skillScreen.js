import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    Image,
    TouchableOpacity,
    StatusBar, 
    FlatList
} from 'react-native';

import { FontAwesome } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import data from './skillData.json';


export default class SkillScreen extends React.Component {

    render(){

        return(
            <View style={styles.container}>
                <StatusBar/>
                
                <View style={styles.header}>

                    <View>
                        <Image source={require('./image/logo.png')} style={{width: "52%", alignSelf:'flex-end'}} />
                        <Text style={{fontFamily:'Roboto', fontSize:16, alignSelf:'flex-end', color:'#3EC6FF', marginTop:-20}}>PORTOFOLIO</Text>
                    </View>

                    <View style={{flexDirection:'row'}}>
                        <FontAwesome name="user-circle" size={32} color="#3EC6FF" />
                        <View style={{flex:1, marginLeft: 10}}>
                            <Text style={{color:'#003366'}}>Hai,</Text>
                            <Text style={{fontWeight:'bold', color:'#003366'}}>Mukhlis Hanafi</Text>
                        </View>
                    </View>

                    <View style={{height:65, borderBottomWidth: 5, borderColor:'#3EC6FF', marginTop: 5}}>
                        <Text style={{color:'#003366', fontSize:50, fontFamily:'Roboto'}}>SKILL</Text>
                    </View>

                    <View style={{height:40, flexDirection:'row', justifyContent:'space-between'}}>
                        <View style={{height:30, backgroundColor:'#B4E9FF', alignSelf:'center', borderRadius:8}}>
                            <Text style={{padding:5, fontWeight:'bold', color:'#003366', fontSize:15}}>Library / Framework</Text>
                        </View>
                        <View style={{height:30, backgroundColor:'#B4E9FF', alignSelf:'center', borderRadius:8}}>
                            <Text style={{padding:5, fontWeight:'bold', color:'#003366', fontSize:15}}>Bahasa Pemograman</Text>
                        </View>
                        <View style={{height:30, backgroundColor:'#B4E9FF', alignSelf:'center', borderRadius:8}}>
                            <Text style={{padding:5, fontWeight:'bold', color:'#003366', fontSize:15}}>Teknologi</Text>
                        </View>
                    </View>

                    <View style={{height:110, backgroundColor:'#B4E9FF', flexDirection:'row', borderRadius:15, elevation:10}}>
                        <MaterialCommunityIcons name="react" size={70} color="#003366" style={{paddingVertical:17, paddingHorizontal:10}}/>
                        <View>
                            <Text style={{paddingLeft:20, paddingTop:10, fontSize:24, fontWeight:'bold', paddingRight:10, color:'#003366'}}>React Native</Text>
                            <Text style={{paddingLeft:20, fontSize:16, color:'#003366'}}>Library / Framework</Text>
                            <Text style={{paddingLeft:20, fontSize:40, fontWeight:'bold', alignSelf:'flex-end', color:'white'}}>50%</Text>
                        </View>
                        <AntDesign name="right" size={70} color="#003366" style={{alignSelf:'center', paddingLeft:40}}/>

                    </View>
                    
                </View>

                <View style={styles.body}>

                </View>

                <View styles={styles.footer}>

                </View>
            </View>
            
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width:350,
        alignSelf:'center'
    }
});