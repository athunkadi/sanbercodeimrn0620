import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    StatusBar,
    Image,
    TouchableOpacity
} from 'react-native';

import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

export default class LoginScreen extends React.Component {
    
      render() {
        return (
          <View style={styles.container}>
              <StatusBar/>
              <Text style={styles.txtJudul}>Tentang Saya</Text>
              <View style={styles.imgProf}>
                <FontAwesome name="user" size={120} color="grey" style={{alignSelf:"center", paddingTop: 10}} />
              </View>
              <Text style={styles.txtJdlname}>Mukhlis Hanafi</Text>
              <Text style={styles.txtJdlname2}>React Native Developer</Text>
              <View style={styles.porto}>
                <Text style={{fontSize:20, color:'#003366', paddingLeft:5}}>Portofolio</Text>
                <View style={{height:0.5, backgroundColor:'#003366', width:335, alignSelf: 'center'}}/>
                <View style={styles.detailPort1}>
                    <FontAwesome name="gitlab" size={40} color="#3EC6FF" />
                    <AntDesign name="github" size={40} color="#3EC6FF" />
                </View>
                <View style={styles.detailPort2}>
                    <Text style={{alignSelf:'center', color:'#003366', fontSize: 15, fontWeight:'bold'}}>@mukhlish</Text>
                    <Text style={{alignSelf:'center', color:'#003366', fontSize: 15, fontWeight:'bold'}}>@mukhlis-h</Text>
                </View>
              </View>
              <View style={styles.porto2}>
                <Text style={{fontSize:20, color:'#003366', paddingLeft:5}}>Hubungi Saya</Text>
                <View style={{height:0.5, backgroundColor:'#003366', width:335, alignSelf: 'center'}}/>
                <View style={styles.detailPort3}>
                    <FontAwesome name="facebook-square" size={40} color="#3EC6FF" />
                    <Text style={{alignSelf:'center', color:'#003366', fontSize: 15, fontWeight:'bold'}}>mukhlis.hanafi</Text>
                </View>
                <View style={styles.detailPort3}>
                    <AntDesign name="instagram" size={40} color="#3EC6FF" />
                    <Text style={{alignSelf:'center', color:'#003366', fontSize: 15, fontWeight:'bold'}}>@mukhlis.hanafi</Text>
                </View>
                <View style={styles.detailPort3}>
                    <AntDesign name="twitter" size={40} color="#3EC6FF" />
                    <Text style={{alignSelf:'center', color:'#003366', fontSize: 15, fontWeight:'bold'}}>@mukhlish</Text>
                </View>
              </View>
          </View>
        );
      }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    txtJudul: {
        fontSize: 40,
        textAlign: 'center',
        marginTop: 10,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        color: '#003366'
    },
    txtJdlname: {
        fontSize: 20,
        textAlign: 'center',
        marginTop: 10,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        color: '#003366' 
    },
    txtJdlname2: {
        fontSize: 13,
        textAlign: 'center',
        marginTop: 1,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        color: '#3EC6FF' 
    },
    imgProf: {
        backgroundColor: '#EFEFEF',
        height: 150, 
        width: 150, 
        alignSelf: 'center', 
        borderRadius:100,
        marginTop: 10
    },
    porto: {
        backgroundColor: '#EFEFEF',
        height: 120,
        width: 350,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 10
    },
    porto2: {
        backgroundColor: '#EFEFEF',
        height: 180,
        width: 350,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 10
    },
    detailPort1: {
        flexDirection: "row",
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingLeft: 45,
        paddingRight: 45
    },
    detailPort2: {
        flexDirection: "row",
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingLeft: 30,
        paddingRight: 30
    },
    detailPort3: {
        flexDirection: "row",
        justifyContent: 'space-between',
        paddingTop: 10,
        paddingLeft: 105,
        paddingRight: 105
    },
});