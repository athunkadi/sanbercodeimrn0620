import React from 'react';
import {
    View,
    Text,
    TextInput,
    StyleSheet,
    StatusBar,
    Image,
    TouchableOpacity
} from 'react-native';

export default class LoginScreen extends React.Component {
    
      render() {
        return (
          <View style={styles.container}>

              <StatusBar/>

              <View style={styles.title}>
                <Image source={require('../images/logo.png')} style={{width:350, height:100}} />
                <Text style={styles.txtTittle}>PORTOFOLIO</Text>
              </View>

              <View style={styles.body}>
                  <Text style={styles.txtBody}>Login</Text>
                  <Text style={styles.txtForm}>Username / Email</Text>
                  <TextInput style={styles.form}/>
                  <Text style={styles.txtForm}>Password</Text>
                  <TextInput style={styles.form}/>
                  <TouchableOpacity>
                      <View style={styles.btn}>
                        <Text style={{color:'white', alignSelf: 'center', marginTop: 10, fontSize: 25}}>Masuk</Text>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                        <Text style={{color:'#3EC6FF', alignSelf: 'center', marginTop: 15, fontSize: 25}}>atau</Text>
                  </TouchableOpacity>
                  <TouchableOpacity>
                      <View style={styles.btn2}>
                        <Text style={{color:'white', alignSelf: 'center', marginTop: 10, fontSize: 25}}>Daftar ?</Text>
                      </View>
                  </TouchableOpacity>
                  
              </View>
            
          </View>
        );
      }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    title: {
        backgroundColor: 'white',
        height: 125,
    },
    txtTittle: {
        fontSize: 28,
        fontFamily: 'Roboto',
        textAlign: 'right',
        marginRight: 30,
        marginTop: -25,
        color: '#3EC6FF'
    },
    body: {
        backgroundColor: 'white',
        height: 450,
        marginTop: 25,
        width: 300,
        alignSelf: 'center'
    },
    txtBody: {
        textAlign: 'center',
        fontSize: 28,
        fontFamily: 'Roboto',
        color: '#003366'
    },
    txtForm: {
        textAlign: 'left',
        fontSize: 20,
        fontFamily: 'Roboto',
        color: '#003366',
        marginTop: 30
    },
    form: {
        height: 40,
        borderWidth: 1,
        borderColor: '#003366'
    },
    btn: {
        marginTop: 35,
        height: 50,
        width: 130,
        alignSelf: 'center',
        backgroundColor: '#3EC6FF',
        borderRadius: 20
    },
    btn2: {
        marginTop: 20,
        height: 50,
        width: 130,
        alignSelf: 'center',
        backgroundColor: '#003366',
        borderRadius: 20
    }
});