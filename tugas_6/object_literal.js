console.log("soal no 1")
var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(arr) {
    // Code di sini 
  var data = []
  
  if(arr.length == 0) {
    return ""
  }else {
    for(var index = 0; index < arr.length; index++){
      var fullname = arr[index][0].concat(' ', arr[index][1], ':')
      fullname += "{\n}".concat('firstName: ', arr[index][0])
      fullname += "{\n}".concat('lastName: ', arr[index][1])
      fullname += "{\n}".concat('gender: ', arr[index][2])
      if(arr[index][3] == null || thisYear - arr[index][3] < 0){
        fullname += "{\n}".concat('age: ','Invalid Birth Year', '\n')
      }else
      fullname += "{\n}".concat('age: ', thisYear - arr[index][3], '\n')
      data.push(fullname)
    }
    return console.log(data)
  }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
